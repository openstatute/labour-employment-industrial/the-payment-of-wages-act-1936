https://clc.gov.in/clc/acts-rules/payment-wages-0 
THE PAYMENT OF WAGES ACT

**THE PAYMENT OF WAGES ACT, 1936along with**  
**The Payment of Wages (Procedure) Rules, 1937**  
CONTENTS

[Introduction](#INTRODUCTION)  
**SECTIONS**  
**1\. [Short title, extent, commencement and application](#Short title, extent, commencement and application).**  
**2. [Definitions](#Definitions).**  
**3\. [Responsibility for payment of wages.](#Responsibility for payment of wages)**  
**4\. [Fixation of wage-periods.](#Fixation of wage-periods-)**  
**5\. [Time of payment of wages.](#Time of payment of wages)**  
**6\. [Wages to be paid in current coin or currency notes.](#Wages to be paid in current coin or currency notes)**  
**7\. [Deduction which may be made from wages.](#Deductions which may be made from wages)**  
**8.[Fines.](#8. Fines)**  
**9\. [Deductions for absence from duty.](#Deductions for absence from duty)**  
**10\. [Deductions for damage or loss.](#Deductions for damage or loss)**  
**11.[Deductions for services rendered.](#Deductions for services rendered)**  
**12.[Deductions for recovery of advances.](#Deductions for recovery of advances)**  
**12A. [Deductions for recovery of loans](#Deductions for recovery of loans-Deductions for).**  
**13.[Deductions for payments to co-operative societies and insurance schemes](#Deductions for payments to co-operative societies and insurance schemes).**  
**13A. [Maintenance of registers and records.](#Maintenance of registers and records)**  
**14. [Inspectors.](#14. Inspectors)**  
**14A. [Facilities to be afforded to Inspectors](#14A. Facilities to be afforded to Inspectors).**  
**15\. [Claims arising out of deductions from wages or delay in payment of wages and penalty for malicious or vexatious claims](#Claims arising out of deductions from wages or delay in payment of wages and penalty for malicious or vexatious claims).**  
**16\. [Single application in respect of claims from unpaid group.](#16. Single application in respect of claims from unpaid group)**  
**17.[Appeal.](#17. Appeal)**  
**17A.[Conditional attachment of property of employer or other person responsible for payment of wages.](#17A. Conditional attachment of property of employer or other person responsible for payment of wages)**  
**18\. [Powers of authorities appointed under section 15.](#18. Powers of authorities appointed under section 15)**  
**19\. [\[_Repealed_\]](#19. [Power to recover from employer in certain cases.)**  
**20\. [Penalty for offences under the Act.](#20. Penalty for offences under the Act)**  
**21\. [Procedure in trial of offences.](#21. Procedure in trial of)**  
**22.[Bar of suits.](#22. Bar of suits)**  
**22A. [Protection of action taken in good faith.](#[22A. Protection of action taken in good faith)**  
**23. [Contracting out.](#23. Contracting out)**  
**24\. [Application of Act to railways, air transport services, mines and oilfields.](#24. Application of Act to railways, air transport services, mines and oilfields)**  
**25\. [Display by notice of abstracts of the Act.](#25. Display by notice of abstracts of the Act-)**  
**25A. [Payment of undisbursed wages in cases of death of employed person.](#[25A. Payment of undisbursed wages in cases of death of employed person)**  
**26\. [Rule-making power](#26. Rule-making power)**  
**27\. [THE PAYMENT OF WAGES (PROCEDURE) RULES, 1937](#THE PAYMENT OF WAGES (PROCEDURE) RULES, 1937)**

**THE PAYMENT OF WAGES ACT, 1936**

**INTRODUCTION**

With the growth of industries in India, problems relating to payment of wages to persons employed in industry took an ugly turn. The Industrial units were not making payment of wages to their workers at regular intervals and wages were not uniform. The industrial workers were forced to raise their heads against their exploitation.

The Payment of Wages Bill, 1935, based upon the same principle as the earlier Bill of 1933 but thoroughly revised was introduced in the Legislative Assembly on 15th February, 1935. The Bill was referred to the Select Committee. The Select Committee presented its report, with the amended Payment of Wages Bill, 1935 to the Legislative Assembly on 2nd September, 1935.

**STATEMENT OF OBJECTS AND REASONS**

In 1926 the Government of India addressed Local Governments with a view to ascertain the position with regard to the delays which occurred in the payment of wages to persons employed in industry, and the practice of imposing fines on them. The investigations revealed the existence of abuses in both directions and the material collected was placed before the Royal Commission on Labour which was appointed in 1929. The Commission collected further evidence on the subject and the results of their examination with their recommendations will be found on pages 216--221 and 236--241 of their Report. The Government of India re-examined the subject in the light of the Commission's Report and in February 1933 a Bill embodying the conclusions then reached was introduced and circulated for the purpose of eliciting opinion. A motion for the reference of the Bill to a Select Committee was tabled during the Delhi session of 1933 34, but was not reached, and the Bill lapsed, The present Bill is based upon the same principles as the original but has been revised throughout in the light of the criticisms received when the original Bill was circulated.

_[TOP](#CONTENTS)_  
**ACT 4 OF 1936**

The Payment of Wages Bill, 1935 was passed by the Legislative Assembly and it received the assent on 23rd April, 1936. It came on the Statute Book as "THE PAYMENT OF WAGES ACT, 1936 (4 of 1936)".

**LIST OF AMENDING ACTS AND ADAPTATION ORDERS**

1.The Government of India (Adaptation of Indian Laws) Order, 1937.  
2.The Repealing and Amending Act, 1937 (20 of 1937).  
3.The Payment of Wages (Amendment) Act, 1937 (22 of 1937).  
4.The Payment of Wages (Amendment) Ordinance, 1940 (3 of 1940),  
5.The Indian Independence (Adaptation of Central Acts and Ordinances) Order, 1948.  
6.The Adaptation of Laws Order, 1950.  
7.The Part B States (Laws) Act, 1951 (3 of 1951).  
8.The Payment of Wages (Amendment) Act, 1957 (68 of 1957).  
9.The Payment of Wages (Amendment) Act, 1964 (53 of 1964).  
10.The Central Labour Laws (Extension to Jammu and Kashmir) Act, 1970 (51 of 1970).  
11.The Repealing and Amending Act, 1974 (56 of 1974).  
12.The Payment of Wages (Amendment) Act, 1976 (29 of 1976).  
13.The Payment of Wages (Amendment) Act, 1977 (19 of 1977).  
14.The Payment of Wages (Amendment) Act, 1982 (38 of 1982).

_[TOP](#CONTENTS)_ **THE PAYMENT OF WAGES ACT, 1936**  
**(4 OF 1936)**  
\[23rd April, 1936\]  
_An Act to regulate the payment of wages of certain classes of \[employed persons\]_

WHEREAS it is expedient to regulate the payment of wages to certain classes of persons employed in industry.  
it is hereby enacted as follows:-

**1\. Short title, extent, commencement and application-**(1) This Act may be called the Payment of Wages Act, 1936.  
(2) It extends to the whole of India  
(3) It shall come into force on such dateas the Central Government may by notification in the Official Gazette, appoint.  
(4) It applies in the first instance to the payment of wages to persons employed in any \[factory, to persons\] employed (otherwise than in a factory) upon any railway by a railway administration or, either directly or through a sub-contractor, by a person fulfilling a contract with a railway administration, \[and to persons employed in an industrial or other establishment specified in sub-clauses (a) to (g) of clause (ii) of  
Section 2.\]  
(5) The State Government may, after giving three months' notice of its intention of so doing, by notification in the Official Gazette, extend the provisions of \[this Act\] or any of them to the payment of wages to  
any class of persons employed in \[ any establishment or class of establishments specified by the Central Government or a State Government under sub-clause (h) of clause (ii) of Section 2:\]  
\[provided that in relation to any such establishment owned by the Central Government no such notification shall be issued except with the concurrence of that Government.\]  
(6) Nothing in this Act shall apply to wages Payable in respect of a wage-period, over such wage-period, average \[one thousand six hundred rupees\] a month or more.

_[TOP](#CONTENTS)_  
**2\. Definitions.** In this Act, unless there is anything repugnant in the subject or context,-  
(i) "employed person" includes the legal representative of a deceased person;  
(ia) "employer" includes the legal representative of a deceased employer:  
(ib) "factory" means a factory as defined in clause (m) of section 2 of the Factories Act, 1948 (63 of 1948) and includes any place to which the provisions of that Act have been applied under sub-section (1) of section 85 thereof; \]  
(ii) \["industrial or other establishment" means\] any

(a) tramway service, or motor transport service engaged in carrying passenger or goods or both by road for hire or reward;  
(aa) air transport service other than such service belonging to, or exclusively employed in the military, naval or air forces of the Union or the Civil Aviation Department of the Government of India;\]  
(b) dock, wharf or jetty;  
_[TOP](#CONTENTS)_  
(C) inland vessel, mechanically propelled:  
(d) mine, quarry on oil-field;  
(e) plantation;  
(f) workshop or other establishment in which articles are produced, adapted or manufactured, with a view to their use, transport or sale;  
(g) establishment in which any work relating to the construction, development or maintenance of buildings, roads, bridges or canals, or relating to operation connected with navigation, irrigation, development or maintenance of buildings roads, bridges or mission and distribution of electricity or any other form of power is being carried on\](h) any other establishment or class of establishment which the Central Government or a State Government may, having regard to the nature thereof, the need for protection of persons employed therein and other relevant circumstances, specify, by notification in the Official Gazette;  
(iia) "mine" has the meaning assigned to it in clause (j) of sub-section (1) of section 2 of the Mines Act, 1952 (35 of 1952)

_[TOP](#CONTENTS)_

(iii) "plantation" has the meaning assigned to it in clause (f) of section 2 of the Plantations Labour Act, 1951 (69 of 1951)  
(iv) "prescribed" means prescribed by rules made under this Act  
(v) "railway administration" has the meaning assigned to it in clause (6) of section 3 of the Indian Railways Act, 1890 (9 of 1890); and  
(vi) "wages" means all remuneration (whether by way of salary, allowances, or otherwise) expressed in terms of money or capable of being so expressed which would, if the terms of employment, express or implied, were" fulfilled, be payable to a person employed in respect of his employment or of work done in such employment, and includes-  
(a)any remuneration payable under any award or settlement between the parties or order of a Court;  
(b)any remuneration to which the person employed is entitled in respect of overtime work or holidays or any leave period;  
(c)any additional remuneration payable under the terms of employment (whethercal1ed a bonus or by any other name);  
(d)any sum which by reason of the termination of employment of the person employed is payable under any law, contract or instrument which  
provides for the payment of such sum, whether with or without deductions, but does not provide for the time within which the payment is to be made;  
(e)any sum to which the person employed is entitled under any scheme framed under any law for the time being in force

but does not include-  
(1)any bonus (whether under a scheme of profit sharing or otherwise) which does not form part of the remuneration payable under the terms of employment or which is not payable under any award or settlement between the parties or order of a Court;  
(2)the value of any house-accommodation, or of the supply of light, water, medical attendance or other amenity or of any service excluded from the computation of wages by a general or special order of the State Government;  
(3)any contribution paid by the employer to any pension or provident fund, and the interest which may have accrued thereon;  
(4)any travelling allowance or the value of any travelling concession:  
(5)any sum paid to the employed person to defray special expenses entailed on him by the nature of his employment; or  
(6)any gratuity payable on the termination of employment in cases other than those specified in sub-clause (d).\]  
 

**3\. Responsibility for payment of wages-** Every employer shall be responsible for the payment to persons employed by him of all wages required to be paid under this Act:  
Provided that, in the case of persons employed (otherwise than by a contractor)-  
(a)in factories, if a person has been named as the manager of the factory under \[clause (t) of sub-section (1) of section 7 of the Factories Act, 1948 (63 of 1948)\]  
(b) in industrial or other establishments, if there is a person responsible to the employer for the supervision and control of industrial or other establishments  
(C)upon railways (otherwise than in factories), if the employer is the railway administration and the railway administration has nominated a person in this behalf for the local area concerned.  
the person so named, the person responsible to the employer, or the person so nominated, as the case may be \[shall also be responsible\] for such payment.

**4\. Fixation of wage-periods-** (1) Every person responsible for the payment of wages under section 3 shall fix periods (in this Act referred to as wage-periods) in respect of which such wages shall be payable.  
(2) No wage-period shall exceed one month.

**5\. Time of payment of wages-** (1) The wages of every person employed upon or in-

(a)any railway, factory or \[industrial or other establishment\] upon or in which less than one thousand persons are employed, shall be paid before expiry of the seventh day,  
(b)any other railway, factory or \[industrial or other establishment\], shall be paid before the expiry of the tenth day,  
after the last day of the wage-period in respect of which the wages are payable:  
Provided that in the case of persons employed on a dock, wharf or jetty or in a mine, the balance of wages found due on completion of the final tonnage account of the ship or wagons loaded or unloaded, as the case may be, shall be paid before the expiry of the seventh day from the day of such completion.  
(2) Where the employment of any person is terminated by or on behalf of the employer, the wages, earned by him shall be paid before the expiry of the second working day from the day on which his employment is terminated:  
provided that where the employment of any person in an establishment is terminated due to the closure of the establishment for any reason other than a weekly or other recognised holiday, the wages earned by him shall be paid before the expiry of the second day from the day on which his employment is so terminated.  
(3) The State Government may, by general or special order, exempt, to such extent and subject to such conditions as may be specified in the order, the person responsible for the payment of wages to persons employed upon any railway (otherwise than in a factory)\[or to persons employed as daily-rated workers in the Public Works Department of the Central Government or the state Government from the operation of this section in respect of the wages on any such persons or class of such persons:

provided that in the case of persons employed as daily-rated workers as aforesaid, no such order shall be made except in consultation with the Central Government.  
(4) \[Save as otherwise provided in sub-section (2), all payments\] of wages shall be made on a working day.

_[TOP](#CONTENTS)_  
**6\. Wages to be paid in current coin or currency notes-**All wages shall be paid in current coin or currency notes or in both:  
\[provided that the employer may, after obtaining the written authorisation of the employed person, pay him the wages either be cheque or by crediting the wages in his bank account.\]

**7\. Deductions which may be made from wages-**(1) Not-withstanding the provisions of sub-section (2) of section 47 of the Indian Railways Act, 1890 (9 of 1890), the wages of an employed person shall be paid to him without deductions of any kind except those authorised by or under this Act.

\[Explanation I.\]-Every payment made by the employed person to the employer or his agent shall, for the purposes of this Act, be deemed to be a deduction from wages.  
\[Explanation II.-Any loss of wages resulting from the imposition, for good and sufficient cause, upon a person employed of any of the following penalties, namely:-  
(i)the withholding of increment or promotion (including the stoppage of increment at an efficiency bar):  
(ii)the reduction to a lower post or time scale or to a lower stage in a time scale: or  
(iii)suspension,  
shall not be deemed to be a deduction from wages in any case where the rules framed by the employer for the imposition of any such penalty are in conformity with the requirements, if any, which may be specified in this behalf by the State Government by notification in the Official Gazette.\]

(2) Deductions from the wages of an employed person shall be made only in accordance with the provisions of this Act, and may be of the following kinds only, namely:-  
(a)fines;  
(b)deductions for absence from duty;  
(c)deductions for damage to or loss of goods expressly entrusted to the employed person for custody, or for loss of money for which he is required to account, where such damage or loss is directly attributable to his neglect or default;  
\[(d) deductions for house-accommodation supplied by the employer or by Government or any housing board set up under any law for the time being in force (whether the Government or the board is the employer or not) or any other authority engaged in the business of subsidising house-accommodation which may be specified in this behalf by the State Government by notification in the Official Gazette;\]  
(e) deductions for such amenities and services supplied by the employer as theState Government 3 \[or any officer specified by it in this behalf\] may, by general or special order, authorise;  
_Explanation-_The word "services" in 39\[this clause\] does not include the supply of tools and raw materials required for the purposes of employment;  
\[(f) deductions for recovery of advances of whatever nature (including advances for travelling allowance or conveyance allowance), and the interest due in respect there-of, or for adjustment of over-payments of wages;  
(ff)deductions for recovery of loan made from any fund constituted for the welfare of labour in accordance with the rules approved by the State Government, and the interest due in respect thereof.  
(fff) deductions for recovery of loans granted for house-building or other purposes approved by the State Government and the interest due in respect thereof;\]  
(g) deductions of income-tax payable by the employed person:

(h)deductions required to be made by order of a Court or other authority competent to make such order;  
(i)deductions for subscriptions to, and for repayment of advances from any provident fund to which the Provident Funds Act, 1925 (19 of 1925), applies or any recognised provident fund as defined in section 58A of the Indian income-tax Act, 1922 (11 of 1922), or any provident fund approved in this behalf by the State Government, during the continuance of such approval;  
(j) deductions for payments to co-operative societies approved by the State Government \[or any officer specified by it in this behalf\] or to a scheme of insurance maintained by the Indian Post Office; \[and\]  
\[(k) deductions, made with the written authorisation of the person employed for payment of any premium on his life insurance policy to the Life Insurance Corporation of India established under the Life Insurance Corporation Act, 1956 (31 of 1956), or for the purchase of securities of the Government of India or of any State Government or for being deposited in any Post Office Savings Bank in furtherance of any savings scheme of any such Government.\]\]  
\[(kk) deductions made, with the written authorisation of the employed person, for the payment of his contribution to any fund constituted by the employer or a trade union under the Trade Union Act, 1926 (16 of 1926), for the welfare of the employed persons or the members of their families, or both, and approved by the State Government or any officer specified by it in this behalf, during the continuance of such approval;  
(kkk)deductions made, with the written authorisation of the employed person, for payment of the fees payable by him for the membership of any trade union registered under the Trade Unions Act, 1926 (16 of 1926);\]

_[TOP](#CONTENTS)_  
\[(l) deductions, for payment of insurance premia on Fidelity Guarantee Bonds;  
(m)deductions for recovery of losses sustained by a railway administration on account of acceptance by the employed person of counterfeit or base coins or mutilated or forged currency notes;  
(n)deductions for recovery of losses sustained by a railway administration on account of the failure of the employed person to invoice, to bill, to collect or to account for the appropriate charges due to that administration; whether in respect of fares, freight, demurrage, wharfage and cranage or in respect of sale of food in catering establishments or in respect of sale of commodities in grain shops or otherwise;  
(o) deductions for recovery of losses sustained by a railway administration on account of any rebates or refunds incorrectly granted by the employed person where such loss is directly attributable to his neglect or default;\]  
\[(p) deductions, made with the written authorisation of the employed person, for contribution to the Prime Minister's National Relief Fund or to such other Fund as the Central Government may, by notification in the Official Gazette, specify;\]  
(q) deductions for contributions to any insurance scheme framed by the Central Government for the benefit of its employees.\]  
\[(3) Notwithstanding anything contained in this Act, the total amount of deductions which may be made under sub-section (2) in any wage-period from the wages of any employed person shall not exceed-  
(i)in cases where such deductions are wholly or partly made for payments to co-operative societies under clause (j) of sub-section (2), seventy-five per cent of such wages, and

_[TOP](#CONTENTS)_  
(ii)in any other case, fifty percent of such wages;  
provided that where the total deductions authorised under sub-section (2) exceed seventy five percent, or as the case may be, fifty percent, of the wages, the excess may be recovered in such manner as may be prescribed.  
(4) Nothing contained in this section shall be construed as precluding the employer from recovering from the wages of the employed person or otherwise any amount payable by such person under any law for the time being in force other than the Indian Railways Act, 1890 (9 of 1890).\]

**COMMENTS**

An employer can deduct the wages under section 7(2) (b) of the Act for absence from duty. Absence from duty by an employee must be on his own volition and it cannot cover his absence when he is forced by circumstances created by the employer from carrying out his duty. In the case in hand as the absence of the employees was not vountary in as much as, they were not allowed to resume their work without signing the guarantee bound no deduction can be made under the Act. \[_French Motor Car Co. Ltd. Workers Union v. French Motor Car Co. Ltd;_ LLR 1990 page 366\]. If the workman did not work, although the work was offered to him, he is not entitled to wages. \[Modi Industries v. State of U.P., (1992) 64 FLR 471 (All.)\] If the absence from duty is due to coercion and the workman is not a consenting party, then the management has no power to deduct wages. (_Kothari (Madras) Ltd. v. Second Addl. District Judge-cum. Appellate Authority & Ors;_ (1990) 76 FJR 209(A.P.)\]. It is too well-settled that "go-slow" is a serious misconduct being a covert and a more damaging breach of the contract of employment. (_Bank of India v. T.S. Kelawala & Ors;_ 1990 LLR 313 (S.C.)\].  
The workman cannot be denied the wages when he reports himself on duty but the work is not taken from him by the employer. \[_J.D.A. v. Labour Centre & Ors;_ (1990) 60 F.L.R. 81 (Raj.)\]. Compensation up to 10 times cannot be granted incase of back wages awarded by the Industrial Tribunal. \[_Municipal Council & Ors. v. Khubilal,_ (1992) 64 F.L.R. 752 (Raj.)\]. The Prescribed Authority has been cenferred power to entertain the application even beyond the period of 12 months. (_Rahat Hussain Khan v. 3rd Addl. District Judge & Ors;_ (1992) 64F.L.R. 302 (All.)\]. Appeal is not made to a _person designata_ but to a court. Revision lies against the appellate order to the High Court. \[_Bharatpur Central Co-op. Bank Ltd. v. Rattan Singh & Ors;_ (1990) II C.L.R. 516 (Raj.)\]. The requirement of making deposit at the time of filing of appeal does not destroy the remedy of the appeal. \[_Nagar Palika v. Prescribed Authority & Ors;_ (1992) 64 F.L.R. 1005 (All.)\].

_[TOP](#INTRODUCTION)_  
**8\. Fines-**(1) No fine shall be imposed on any employed person save in respect of such acts and omissions on his part as the employer, with the previous approval of the State Government or of the prescribed authority, may have specified by notice under sub-section (2).  
(2) A notice specifying such acts and omissions shall be exhibited in the prescribed manner on the premises in which the employment is carried on or in the case of person employed upon a railway (otherwise than in a factory), at the prescribed place or places.  
(3) No fine shall be imposed on any employed person until he has been given an opportunity of showing cause against the fine, or other-wise, than in accordance with such procedure as may be prescribed for the imposition of fines.  
(4) The total amount of fine which may be imposed in any one wage period on any employed person shall not exceed an amount equal to\[three per cent\] of the wages payable to him in respect of that wage-period.  
(5) No fine shall be imposed on any employed person who is under the age of fifteen years.  
(6) No fine imposed on any employed person shall be recovered from him by instalments or after the expiry of sixty days from the day on which it was imposed.  
(7) Every fine shall be deemed to have been imposed on the day of the act or omission in respect of which it was imposed.  
(8) All fines and all realisations thereof shall be recorded in a register to be kept by the person responsible for the payment of wages under section 3 in such form as may be precribed; and all such realisations shall be applied only to such purposes beneficial td the persons employed in the factory or establishment as are approved by the prescribed authority.

_Explanation.-_When the persons employed upon or in any railway, factory or \[industrial or other establishment\] are part only of a staff employed under the same management, all such realisations may be credited to a common fund maintained for the staff as a whole, provided that the fund shall be applied only to such purposes as are approved by the prescribed authority.

**9\. Deductions for absence from duty-**(1) Deductions may be made under clause (b) of sub-section (2) of section 7 only on account of the absence of an employed person from the place or places where, by the terms of his employment, he is required to work, such absence being for the whole or any part of the period during which he is so required to work.  
(2) The amount of such deduction shall in no case bear to the wages payable to the employed person in respect of the wage-period for which the deduction is made a large proportion than the period for which he was absent bears to the total period, within such wage-period, during which by the terms of his employment, he was required to work:  
Provided that, subject to any rules made in this behalf by the State Government, if ten or more employed persons acting in concert absent themselves without due notice (that is to say without giving the notice which is required under the terms of their contracts of employment) and without reasonable cause, such deduction from any such person may include such amount not exceeding his wages for eight days as may by any such terms be due to the employer in lieu of due notice.

\[_Explanation._\-For the purposes of this section, an employed person shall be deemed to be absent from the place where he is required to work if, although present in such place, he refuses, in pursuance of a stay-in strike or for any other cause which is not reasonable in the circumstances, to carry out his work.\]

**COMMENTS**

The workman cannot be denied the wages when he reports himself on duty but the work is not taken from him by the employer. \[_J.D.A. v. Labour Centre & Ors;_ (1990) 60 F.L.R. 81 (Raj.)\] Compensation upto 10 times cannot be granted in case of back wages awarded by the Industrial Tribunal. \[_Municipal Council & Ors. v. Khubilal,_ (1992) 64 F.L.R. 752 (Raj.)\]. The Prescribed Authority has been cenferred power to entertain the application even beyond the period of 12 months. \[_Rahat Hussain Khan v. 3rd Addl. District Judge & Ors;_ (1992) 64 F.L.R. 302 (All.)\]. Appeal is not made to a persona designata but to a court. Revision lies against the appellate order to the High Court. \[_Bharatpur Central Co-op. Bank Ltd. v. Rattan Singh & Ors;_ (1990) II C.L.R. 516 (Raj.)\] .The requirement of making deposit at the time of filing of appeal does not destroy the remedy of the appeal. \[_Nagar Palika v. Prescribed Authority & Ors;_ (1992) 64 F.L.R. 1005 (All.)\].

**10\. Deductions for damage or loss-**\[(1) A deduction under clause (c) or clause (o) of sub-section (2) of section 7 shall not exceed the amount of the damage or loss caused to the employer by the neglect or default of the employed person.

(1A) A deduction shall not be made under clause (c) or clause (m) or clause (n) or clause (o) of sub-section (2) of section 7 until the employed person has been given an opportunity of showing cause, against the deduction, or otherwise than in accordance with such procedure as may be prescribed for the making of such deductions.\]  
(2) All such deductions and all realisations thereof shall be' recorded in a register to be kept by the person responsible for the payment of wages under section 3 in such form as may be prescribed.

**11\. Deductions for services rendered**\-A deduction under clause (d) or clause (e) of sub-section (2) of section 7 shall not be made from the wages of an employed person, unless the house-accommodation amenity or service has been accepted by him, as a term of employment or otherwise, and such deduction shall not exceed an amount equivalent to the value of the house-accommodation amenity or service supplied and, in the case of deduction under the said clause (e), shall be subject to such conditions as the State Government may impose.

**12\. Deductions for recovery of advances-**Deductions under clause (f) of sub-section (2) of section 7 shall be subject to the following conditions, namely:-

(a)recovery of an advance of money given before employment began shall be made from the first payment of wages in respect of a complete wage-period, but no recovery shall be made of such advances given for travelling-expenses;  
\[(aa) recovery of an advance of money given after employment began shall be subject to such conditions as the State Government may impose;\]  
(b)recovery of advances of wages not already earned shall be subject to any rules made by the State Government regulating the extent to which such advances may be given and the instalments by which they may be recovered.  
 

**\[12A. Deductions for recovery of loans-Deductions for:,** recovery of loans granted under clause (fff) of sub-section (2) of section' 7 shall be subject to any rules made by the State Government regulating the extent to which such loans may be granted and the 'fate of interest payable thereon.\]

**13\. Deductions for payments to co-operative societies and insurance schemes.-**Deductions under clause (j) \[and clause (k)\] of sub-section (2) of section 7 shall be subject to such conditions as the State' Government may impose.

**\[13A. Maintenance of registers and records**\-(1) Every employer shall maintain such registers and records giving such particulars of persons employed by him, the work performed by them, the Wages paid to them, the deductions made from their wages, the receipts given by them and such other particulars and in such form as may be prescribed.  
(2) Every register and record required to be maintained under this' section shall, for the purposes of this Act, be preserved for a period of-three years after the date of the last entry made therein.\].

**14\. Inspectors-**(1) An Inspector of Factories appointed under \[sub-section (1) of section 8 of the Factories Act, 1948 (63 of 1948)\], shall be an Inspector for the purposes of this Act in respect of all factories within the local limits assigned to him.  
(2) The State Government may appoint Inspectors for the purposes of this Act in respect of all persons employed upon a railway (otherwise than in a factory) to whom this Act applies.  
(3) The State Government may, by notification in the Official Gazette, appoint such other persons as it thinks fit to be Inspectors for the purposes of this Act, and may define the local limits within which and the class of factories and\[industrial or other establishments\] in respect of which they shall exercise their functions.  
\[(4) An Inspector may,-  
(a)make such examination and inquiry as he thinks fit in order to ascertain whether the provisions of this Act. or rules made thereunder are being observed;  
(b)with such assistance, if any as he thinks fit, enter, inspect and search any premises of any railway, factory or \[industrial or other establishment\] at any reasonable time for the purpose of carrying out the object of this Act:  
(c)supervise the payment of wages to persons employed upon any railway or in any factory or \[industrial or other establishment;\]  
(d)require by a written order the production at such place, as may be prescribed, of any register maintained in pursuance of this Act and take on the spot or otherwise statements of any persons which he may consider necessary for carrying out the purposes of this Act;  
(e)seize or take copies of such registers or documents or portions thereof as he may consider relevant in respect of an offence under this Act which he has reason to believe has been committed by an employer;  
(f)exercise such other powers as may be prescribed.

Provided that no person shall be compelled under this sub-section to answer any question or make any statement tending to incriminate himself.  
(4A) The provisions of the \[Code of Criminal Procedure, 1973 (2 of 1974)\] shall, so far as maybe, apply to any search or seizure under this sub-section as they apply to any search or seizure made under the authority of a warrant issued under 66\[section 94\] of the said Code.\]

(5) Every Inspector shall be deemed to be a public servant within the meaning of the Indian Penal Code (45 of 1860).

**\[14A. Facilities to be afforded to Inspectors**\-Every employer shall afford an Inspector all reasonable facilities for making any entry, inspection, supervision, examination or inquiry under this Act.\]

**15\. Claims arising out of deductions from wages or delay in payment of wages and penalty for malicious or vexatious claims-**(1) The State Government may, by notification in the Official Gazette, appoint \[a presiding officer of any Labour Court or Industrial Tribunal, constituted under the Industria1Disputes Act, 1947 (14 of 1947), or under any corresponding law relating to the investigation and settlement of industrial disputes in force in the State or\] any Commission for Workmen's Compensation or other officer with experience as a Judge of a Civil Court or as a stipendiary Magistrate to be the authority to hear and decide for any specified area all claims arising out of deductions from the wages, or delay in payment of the wages, \[ of persons employed or paid in that area, including all-matters, incidental to such claims:

Provided that where the State Government considers it necessary so to do, it may appoint more than one authority for any specified area and may, by general or special order, provide for the distribution or allocation of work to be performed by them under this Act.\]  
(2) Where contrary to the provisions of this Act any deduction has been made from the wages of an employed person, or any payment of wages has been delayed, such person himself, or any legal practitioner or any official of a registered trade union authorised in writing to act on his behalf, or, any Inspector under this Act, or any other person acting with the permission of the authority appointed under sub-section (1), may apply to such authority for a direction under sub-section (3):  
Provided that every such application shall be presented within \[twelve months\] from the date on which the deduction from the wages was made or from the date on which the payment of the wages was due to be made, as the case may be:

Provided further that any application may be admitted after the said period satisfies the authority that he had sufficient cause for not making the application within: such period.  
(3) When any application under sub-section (2) is entertained, the authority shall hear the applicant and the employer or other person responsible for the payment of wages under section 3, or give them an opportunity of being heard, and, after such further inquiry (if any) as maybe necessary, may, without prejudice to any other penalty to which such employer or other person is liable under this Act, direct the refund to the employed person of the amount deducted, or the payment of the delayed wages, together with the payment of such compensation as the authority may think fit, not exceeding ten times the amount deducted in the former case and\[not exceeding twenty-five rupees in the latter, and even if the amount deducted or the delayed wages are paid before the disposal of the application, direct the payment of such compensation, as the authority may think fit, not exceeding twenty-five rupees\]:

Provided that no direction for the payment of compensation shall be made in the case of delayed wages if the authority is satisfied that the delay was due to-

(a)a bona fide error or bona fide dispute as to the amount payable to the employed person, or  
(b)the occurrence of an emergency, or the existence of exceptional circumstances, such that the person responsible for the payment of the wages was unable, though exercising reasonable diligence, to make prompt payment, or  
(c)the failure of the employed person to apply for or accept payment.  
\[(4) If the authority hearing an application under this section is satisfied-  
(a)that the application was either malicious or vexatious, the authority may direct that a penalty not exceeding fifty rupees be paid to the employer or other person responsible for the payment of wages by the person presenting the application; or  
(b)that in any case in which compensation is directed to be paid under sub-section (3), the applicant ought not to have been compelled to seek redress under this section, the authority may direct that a penalty not exceeding fifty rupees be paid to the State Government by the employer or other person responsible for the payment of wages.  
(4A) Where there is any dispute as to the person or persons being the legal representative or representatives of the employer or of the employed person, the decision of the authority on such dispute shall be final.  
(48) Any inquiry under this section shall be deemed to be a judicial proceeding within the meaning of sections 193, 219 and 228 of the Indian Penal Code (45 of 1860).\]  
(5) Any amount directed to be paid under this section may be recovered-  
(a)if the authority is a Magistrate, by the authority as if it were a fine imposed by him as Magistrate, and  
(b)if the authority is a Magistrate, by the Magistrate to whom the authority makes application in this behalf, as if it were a fine imposed by such Magistrate.

**COMMENTS**  
Where the company was closed without any proper notice to the 14 workmen and the Workmen claimed wages for the period they were kept out of employment, section 25FFF of the Industrial Disputes Act was not applicable and the claim amounted to wages and not compensation and the authority under the Payment of Wages Act had jurisdiction to determine the same. _Banjarwala Tea Estate v. District Judge;_ 1981 Lab IC 370 (42) FLR 165=1981-1 Lab. L.N .371.  
Also see: A. _V. D' costa v. B.C. Pablic_ AIR 1955 SC 412.  
The Wages court is not at all competent to determine whether Variable D.A. under the agreement is payable to any workmen this being a subject matter for the court constituted under Industrial Disputes Act. The jurisdiction of the Wages Court is to entertain application only in two items of cases, namely, of deductions and fine not authorised under section 7 to 13 and of delay in payment of wages beyong the wage periods fixed under section 4 and the time of payment laid down in section. The question whether the employees are entitled to get variable D.A. or not cannot be treated as deductions, and as such, the Wages Court has no jurisdiction to entertain this claim.  
_French Motor Car Co. Ltd. Workers Union v. French Motor Car Co. Ltd; 1990 LLR page 366. Also see_  
_(i) Sailendra Kumar Dutta v. General manager, Gauhati Refinery; 1973 All L.R. 251._  
_(ii) Wage Inspector, Ujjain v. Suraj mal Mehta: 1969-1 LLJ 762._  
The workman cannot be denied the wages when he reports himself on duty but the work is not taken from him by the employer.\[J.D.A. v. Labour Court &Ors; (1990) 60 FLR 81(Raj.)\] Compensation upto 10 times cannot be granted in case of back wages awarded by the Industrial Tribunal. (Municipal Council & 01.5; v. Khubilal, (1992) 64 FLR 752 (Raj.)\]

**16\. Single application in respect of claims from unpaid group-** (1) Employed persons are said to belong to the same unpaid group if they are borne on the same establishment and if \[deduction shave been made from their wages in contravention of this Act for the same cause and during the same wage-period or periods or if\] their wages for the same wage-period or periods have remained unpaid after the day fixed by section 5.  
(2) A single application may be presented under section 15 on behalf or in respect of any number of employed persons belonging to the same unpaid group, and in such case \[ever person on whose bahalf such application is presented may be awarded maximum compensation to the extent specified in sub-section (3) of section 15\].  
(3) The authority may deal with any number of separate pending applications, presented under section 15 in respect of persons belonging to the same unpaid group, as a single application presented under sub-section (2) of this section, and the provisions of that sub-section shall apply accordingly.

**COMMENTS**  
The Prescribed Authority has been conferred power to entertain the application even beyond the period of 12 months. (_Rahat Hussain Khan v. 3rd Addl. District Judge & Ors;_ (1992) 64 FLR 302 (All.)\].

**17\. Appeal-**(1) \[An appeal against on order dismissing either wholly or in part an application made under sub-section (2) of section 15, or against a direction made under sub-section (3) or sub-section (4) of that section\] may be preferred, within thirty days of the date on which **76**\[the order or direction\] was made, in a Presidency-town before the Court of Small Causes and elsewhere before the District Court-  
(a)by the employer or other person responsible for the payment of wages under section 3, if the total sum directed to be paid by way of wages and compensation exceeds three hundred rupees 78\[or such direction has the effect of imposing on the employer or the other person a financial liability exceeding one thousand rupees\], or  
\[(b) by an employed person or any legal practitioner or any official of a registered trade union authorised in writing to act on his behalf or any Inspector under this Act, or any other person permitted by the authority to make an application under sub-section (2) of section 15, if the total amount of wages claimed to have been withheld from the employed person exceeds twenty rupees or from the unpaid group to which the employed person belongs or belonged exceeds fifty rupees, or\] by any person directed to pay a penalty under \[sub-sec-section (4)\] of Section 15.  
(1A) No appeal under clause (a) of sub-section (1)\] shall lie unless the memorandum of a appeal is accompanied by a certificate by the authority to the effect that the appellant has deposited the amount payable under the direction appealed against.\]  
\[(2) Save as provided in sub-section (1) any order dismissing either wholly or in part an application made under sub-section (2) of section 15, or a direction made under sub-section (3) or sub-section (4) of that section shall be final.\]  
\[(3) Where an employer prefers an appeal an appeal under this section, the authority against whose the appeal has been preferred may, and if so direction by the court referred to in sub-section (1) shall, pending with it.  
(4) The court referred in sub-section (1) may, if it thinks fit, submit any question of law for the decision of the High Court and, if it so does, shall decide the question in conformity with such decision.\]

**COMMENTS**

The Appellate Court under section 17 of the Act, is amenable to revision jurisdiction of the High Court, According to section 17 of the Act, Small Cause Court in a Presidency town and elsewhere the District Courts, are the Appellate authority, Both the Courts are subordinate to the High Court and as such, High Court by exercising revisional powers under section 115 C.P.C. can correct any error of jurisdiction committed by the said Appellate Court. _\[French Motors Car Co. Ltd. Workers Union v. French Motor Car Co. Ltd. LLR 1990 page 366 (Gua.H.C.)\]_  
Appeal is not made to a persona designata but to a court. Revision lies against the appellate order to the High Courts. \[Bharatpur Central Co-op. Bank Ltd. v. Rattan Singh & Ors; (1990) II CLR 516 (Raj).)\]  
The requirement of making deposit at the time of filing of a appeal does not destroy the remedy of the appeal. \[Nagar Palika v. Prescribed Authority & Ors; (1992) 64 FLR 1005 (All.)\].

**\[17A. Conditional attachment of property of employer or other person responsible for payment of wages-**(1) Where any time after an application has been made under sub-section (2) of section 15 the authority, or where at any time after an appeal has been filed under section 17 by an employed person or \[any legal practitioner or any official of a registered trade union authorised in writing to act on his behalf or any Inspector under this Act or any other person permitted by the authority to make an application under sub-section (2) of section 15\] the Court referred to in that section, is satisfied that the employer or other person responsible for the payment of wages under section 3 is likely to evade payment of any amount that may be directed to be paid under section 15 or section 17, the authority or the court, as the case may be, except in cases where the authority or court is of opinion that the ends of justice would be defeated by the delay, after giving the employer or other person an opportunity of being heard, may direct the attachment of so much of the property of the employer or other person responsible for the payment of wages as is, in the opinion of the authority or court, sufficient to satisfy the amount which may be payable under the direction.  
(2) The provisions of the Code of Civil Procedure, 1908 (5 of 1908), relating to attachment before judgment under that Code shall, so far as may be, apply to any order for attachment under sub-section (1).\]

**18\. Powers of authorities appointed under section 15-**Every authority appointed under sub-section (1) of section 15 shall have all the of a Civil Court under the Code of Civil Procedure, 1.908 (5 of 1908), for the purpose of taking evidence and of enforcing the attendance of witnesses and compelling the production of documents, and every such authority shall be deemed to be a Civil Court of all the purposes of section 195 and of\[Chapter XXVI of the. Code of Criminal Procedure, 1973 (2 of 1974).\]

**COMMENTS**

Where an authority passed an order directing a party to produce the documents asked for by another party, the order would be liable to be quashed when the order was in the form of a bold directive to the party to produce the documents asked for by another party and it did not contain any recital about the documents which was to be produced about their relevancy to the controversy which the authority was called upon to decide. \[_Newspapers Ltd, Allahabad v. the State of U.P._ 1982 Lab. IC 776 = 1982(44) FLB.\]

**19\. \[Power to recover from employer in certain cases.\] Rep. by the Payment of Wages (Amendment) Act, 1964 (53 of 1964), s.17 (w.e.f.1st February, 1965).**

**20\. Penalty for offences under the Act-**(1) Whoever being responsible for the payment of wages to an employed person contravenes any of the provisions of any of the following section, namely, \[section 5 except sub-section (4) thereof, section 7, section 8 except sub-section (8) thereof, section 9, section 10 except sub-section (2) thereof, and sections 11 to 13\], both inclusive, shall be punishable with fine \[which shall not be less than two hundred rupees but which may extend to one thousand rupees.\]  
(2) Whoever contravenes the provisions of section 4,\[sub-section (4) of section 5, section 6, sub-section (8) of section 8, sub-section (2) of section 10\] or section 25 shall be punishable with fine which may extend to \[five hundred rupees.\]  
\[(3) Whoever being required under this Act to maintain any records or registers or to furnish any information or return-  
(a)fails to maintain such register or record; or  
(b)willfully refuses or without lawful excuse neglects to furnish such information or return; or  
(c)willfully furnishes or causes to be furnished any information or return which he knows to be false; or  
(d)refuses to answer or wilfully gives a false answer to any question necessary for obtaining any information required to be furnished under this Act,  
shall for each such offence, be punishable with fine \[which shall not be less than two hundred rupees but which may extend to one thousand rupees.\]  
(4) Whoever-  
(a)wilfully obstructs an Inspector in the discharge of his duties under this Act; or  
(b)refuses or wilfully neglects to afford an Inspector any reasonable facility for making any entry, inspection, examination, supervision, or inquiry authorised by or under this Act in relation to any railway, factory of \[industrial or other establishment\]; or  
(c)wilfully refuses to produce on the demand of an Inspector any register or other document kept in pursuance of this Act; or  
(d)prevents or attempts to prevent or does anything which he has any reason to believe is likely to prevent any person from appearing before or being examined by an Inspector acting in pursuance of his duties under this Act; shall be punishable with fine \[which shall not be less than two hundred rupees which may extend to one thousand rupees\]. two hundred rupees but which may extend to one thousand rupees\].  
(5) If any person who has been convicted of any offence punishable under this Act is again guilty of an offence involving contravention of the same provision, he shall be punishable on a subsequent conviction with imprisonment for a term \[which shall not be less than one month but which may extend to six months and with fine which shall not be less than five hundred rupees but which may extend to three thousand rupees\]:  
Provided that for the purpose of sub-section, no cognizance shall be taken of any conviction made more than two years before the date on which the commission of the offence which is being punished came to the knowledge of the Inspector.  
(6) If any person fails or wilfully neglects to pay the wages of any employed person by the date fixed by the authority in this behalf, he shall, without prejudice to any other action that may be taken against him, be punishable with an additional fine which may extend to \[one hundred rupees\] for each day for which such failure or neglect continues.\]

**21\. Procedure in trial of offences-**(1) No Court shall, take cognizance of a complaint against any person for an offence under sub-section (1) of section 20 unless an application in respect of the facts constituting the offence has been presented under section 15 has been granted wholly or in part and the authority empowered under the latter section or the appellate Court granting such application has sanctioned the making of the complaint.  
(2) Before sanctioning the making of a complaint against any per- son for an offence under sub-section (1) of section 20, the authority empowered under section 15 or the appellate Court, as the case may be, shall give such person an opportunity of showing cause against the granting of such sanction, and the sanction shall not be granted if such person satisfies the authority or Court that his default was due to-  
(a)a bona fide error or bona fide dispute as to the amount payable to the employed person, or  
(b)the occurrence of an emergency or the existence of exceptional circumstances, such that the person responsible for the payment of the wages was unable, though exercising reasonable diligence, to make prompt payment, or  
(c)the failure of the employed person to app\]y for or accept payment.

(3) No Court shall take cognizance of a contravention of section 4 or of section 6 or of a contravention of any rules made under section 26 except on a complaint made by or with the sanction of an Inspector under this Act.  
\[(3A) No Court shall take cognizance of any offence punishable under sub-section (3) or sub-section (4) of section 20 except on a com- plaint made by or with the sanction of an Inspector under this Act.\]  
(4) In imposing any fine for an offence under sub-section (1) of section 20 the court shall take into consideration the amount of any compensation already awarded against the accused in any proceedings taken under section 15.

**22\. Bar of suits-**No Court shall entertain any suit for the recovery of wages or of any deduction from wages in so far as the sum so claimed-  
(a)forms the subject of an application under section 15 which has been presented by the plaintiff and which is pending before the authority appointed under that section or of an appeal under section 17; or  
(b)has formed the subject of a direction under section 15 in favour of the plaintiff; or  
(c)has been adjudged, in any proceeding under section 15, not to be owed to the plaintiff; or  
(d)could have been recovered by an application under section 15.

**\[22A. Protection of action taken in good faith-** No suit prosecution or other legal proceeding shaI11ie against the Government or any officer of the Government for anything which is in good faith done or intended to be done under this Act.\]

**23\. Contracting out-**Any contract or agreement, whether made before or after the commencement of this Act, whereby an employed person relinquishes any right conferred by this ActshaI1 benuI1 and void in so far as it purports to deprive him of such right.

_[TOP](#CONTENTS)_  
**\[24. Application of Act to railways, air transport services, mines and oilfields-** The powers by this Act conferred upon the State Government shall, in relation to \[railway\], \[air transport services,\] mines and oilfields; be powers of the Central Government.\]

**25\. Display by notice of abstracts of the Act-** The person responsible for the payment of wages to persons \[employed in a factory or an industrial or other establishment\] shall cause to be\[displayed in such factory or industrial or other establishment a notice containing such abstracts of this Act and of the rules made thereunder in English and in the language of the majority of the persons employed \[in the factory, or industrial or other establishment\] as may be prescribed.

**\[25A. Payment of undisbursed wages in cases of death of employed person.** (1) Subject to the other provisions of the Act, all amounts payable to an employed person as wages shall, if such amounts could not or cannot be paid on account of his death before payment or on account of his whereabouts not being known,-

(a) be paid to the person nominated by him in this behalf in accordance with the rules made under this Act; or  
(b) where no such nomination has been made or where for any reasons such amounts cannot be paid to the person so nominated, be deposited with the prescribed authority who shall deal with the amounts so deposited in such manner as may be prescribed.  
(2) Where, in accordance with the provisions of sub-section (1), all amounts payable to an employed person as wages-  
(a) are paid by the employer to the person nominated by the employed person, or  
(b) are deposited by the employer with the prescribed authority, the employer shall be discharged of his liability to pay those wages.\]

**26\. Rule-making power-**(1) The State Government may make rules to regulate the procedure to be followed by the authorities and Courts referred to in sections 15 and 17.  
(2) The State Government may, (by notification in the Official Gazette, make rules for the purpose of carrying into effect the provisions of this Act.  
(3) In particular and without prejudice to the generality of the foregoing power, rules made under sub-section (2) may-  
(a)require the maintenance of such records, registers, returns and notices as are necessary for the enforcement of the Act

prescribe the form thereof and the particulars to be entered in such registers or records:  
(b)require the display in a conspicuous place or premises where employment is carried on of notices specifying rates of wages payable to persons employed on such premises;  
(c)provide for the regular inspection of the weights, measures and weighing machines used by employers in checking rates of wages payable to persons employed on such premises;  
(d)prescribe the manner of giving notice of the days on which wages will be paid;  
(e)prescribe the authority competent to apaprove under sub-section (1) of section 8 acts and omissions in respect of which fines may be imposed;  
(f)prescribe the procedure for the imposition of fines under section 8 and for the making of the deductions referred to in section 10;  
(g)prescribe the conditions subject to which deductions may be made under the proviso to sub-section (2) of section 9;  
(h)prescribe the authority competent to approve the purposes on which the proceeds of fines shall be expended;  
(i)prescribe the extent to which advances may be made and the instalments by which they may be recovered with reference to clause (b) of section 12;  
\[(ia) prescribe the extent to which loans may be granted and the rate of interest payable thereon with reference to section 12A;  
(ib)prescribe the powers of Inspectors for the purposes of this Act;\]  
(j)regulate the scales of costs which may be allowed in proceedings under this Act;  
(k)prescribe the amount of court-fees payable in respect of any proceedings under this Act;  
(l)prescribe the abstracts to be contained in the notices required by section 25.  
\[(la) prescribe the form and manner in which nominations may be made for the purposes of sub-section (1) of section 25A, the cancellation or variation of any such nomination, or the making of any fresh nomination in the event of the nominee predeceasing the person making nomination, and other matters connected with such nominations;  
(Ib) specify the authority with whom amounts required to be deposited under clause (b) of sub-section (1) of section 25A shall be deposited, and the manner in which such authority shall deal with the amounts deposited with it under that clause;  
\[(m) provide for any other matter which is to be or may be prescribed.\]  
(4) In making any rule under this section the State Government may provide that a contravention of the rule shall be punishable with fine which may extend to two hundred rupees.  
(5) All rules made under this section shall be subject to the condition of previous publication, and the date to be specified under clause (3) of section 23 of the General Clauses Act, 1897 (10 of 1897), shall not be less than three months from the date on which the draft of the proposed rules was published.  
\[(6) Every rule made by the Central Government under this section shall be laid, as soon as may be after it is made, before each House of Parliament while it is in session for a total period of thirty days which may be comprised in one session or in \[two or more successive sessions,\] and if, before tile expiry of the session immediately following the session or the successive sessions aforesaid,\] both Houses agree in making any modification in the rule, of both Houses agree that the rule should not be made, the rule shall thereafter have effect only in such modified form or be of no effect, as the case may be; so, however, that any such modification or annulment shall be without prejudice to the validity of anything previously done under that rule.\]

   
**THE PAYMENT OF WAGES (PROCEDURE) RULES, 1937**

_L. 3067, dated the 24th February; 1937_

In exercise of the powers conferred by sub-section (1) of s. 26 of the Payment of Wages Act, 1936 (4 of 1936), read with s. 22 of the General Clauses Act, 1897 (10 of 1897), the Governor-General-in-Council is pleased to make the following rules, the same having been previously published as required by sub-section (5) of s. 26 of the first named Act, namely:-

**1\. Short title.-**\[(1)\] These rules may be called the Payment of Wages (Procedure) Rules, 1937.  
\[(2) They extend to the whole of India except the State of Jammu and Kashmir.\]

**2\. Definitions.-** In these rules, unless there is anything repugnant in the subject or context,-  
(a)'the Act' means the Payment of Wages Act (4 of 1936);  
(b)'Appeal' means an appeal under section 17;  
(c)'the Authority' means the authority appointed under sub-section (1) of section 15;  
(d)'the Court' means the court mentioned in sub-section (1) of section 17;  
(e)'Employer' includes the persons responsible for the payment of wages under section 3;  
(f)'Section' means a section of the Act;  
(g)'Form' means a form appended to these rules;  
4(gg) 'Record of order or direction' means the record of an order dismissing either wholly or in part an application made under sub-section (2) of section 15 or of a direction made under sub-section (3) or sub-section (4) of that section kept in Form 'F';  
(h)words and expressions defined in the Act shall be deemed to have the same meaning as in the Act.

**3\. Form of application.-** Applications under sub-section (2) of section 15 by or on behalf of an employed person or group of employed persons shall be made in duplicate in Form A, Form B or Form C, as the case may be, one copy of which shall bear such court-fee as may be prescribed.

1.

Published in the Gazette of India, 1937, Pt I, p. 303. The rules are reproduced here as amended up-to-date. These rules also apply to railway, mines and oilfields.

2.

Existing rule 1 renumbered as sub-rule (1) by the Payment of Wages (Procedure) (Amendment) Rules, 1951.

3.

Inserted by the Payment of Wages (Procedure) (Amendment) Rules, 1959.

4.

Ins. by _ibid._

**4\. Authorisation.-** The authorisation to act on behalf of an employed person or persons, under section 15, shall be given by a certificate in Form D, shall be presented to the authority hearing the application and shall form part of the record.

**5\. Permission to appear.-** Any person desiring the permission of the Authority to act on behalf of any employed person or persons shall present to the Authority a brief written statement explaining his interest in the matter, and the Authority shall record an order on the statement which in the case of refusal shall include reasons for the order, and shall incorporate it in the record.

**6\. Presentation of documents.-**(1) Applications or other documents relevant to an application may be presented in person to the Authority at any time during hours to be fixed by the Authority, or may be sent to him by registered post.  
(2) The Authority shall at once endorse, or cause to be endorsed, on each document the date of the presentation or receipt, as the case may be.

**7\. Refusal to entertain application.-**(1) The Authority may refuse to entertain an application presented under rule 6, if after giving the applicant an opportunity of being heard, the Authority is satisfied, for reason to be recorded in writing that:  
(a)the applicant is not entitled to present an application; or  
(b)the application is barred by reason of the provisions in the provisos to sub-section (2) of section 15; or  
(c)the applicant shows no sufficient cause for making a direction under section 15.

(2) The Authority may refuse to entertain an application which is insufficiently stamped or otherwise incomplete and, if he so refuses, shall return it at once with an indication of the defects. If the application is presented again after the defects have been made good, the date of representation shall be deemed to be the date. of presentation for the purpose of the proviso to sub-section (2) of section 15.

**8\. Appearance of parties.-**(1) If the application is entertained, the Authority shall call upon the employer by a notice in Form E to appear before him on a specified date together with all relevant documents and witnesses, if any, and shall inform the applicant of the date so specified.  
(2) If the employer or his representative fails to appear on the specified date, the Authority may proceed to hear and determine the application _exparte._  
(3) If the applicant fails to appear on the specified date, the Authority may dismiss the application:

Provided that an order passed under sub-rule (2) or sub-rule (3) may be set aside and the application re-heard on good cause being shown within one month of the date of the said order, notice being served on opposite party of the date fixed for rehearing.

**9\. Record of proceedings.-**(1) The Authority shall in all cases enter the particulars indicated in Form F and at the time of passing orders shall sign and date the Form.  
(2) In a case where no appeal lies, no further record shall be necessary.  
(3) In a case where an appeal lies, the Authority shall record the substance of the evidence and shall append it under his signature to \[the record of order or direction.\]

**10\. Signature on forms.-** Any form, other than\[the record of order or direction\], which is required by these rules to be signed by the Authority, may be signed under his direction and on his behalf by any officer subordinate to him, appointed by him, in writing for this purpose.

**11\. Exercise of powers.-**In exercising the powers of a Civil Court conferred by section 18 the Authority shall be guided in respect of procedure by relevant orders of the First Schedule of the Code of Civil Procedure, 1908, with such alterations as the Authority may find necessary , not affecting their substance, for adapting them to the matter before him, and save where they conflict with the express provisions of the Act or these rules.

**12\. Appeals.-**\[(1) An appeal shall be preferred in duplicate in the form of a memorandum, one copy of which shall bear the prescribed court-fee, setting forth concisely the grounds of objection to the order dismissing either wholly or in part an application made under sub-section (2) of section 15 or a direction made under sub-section (3) or sub-section (4) of that section, as the case may be, and shall be accompanied by a certified copy of the said order or direction.\]  
(2) When an appeal is lodged a notice shall be issued to the respondent in Form G.  
(3) The Court after hearing the parties and after such further inquiry);"' if any, as it may deem necessary, may confirm, vary, or set aside the \[order or direction\] from which the appeal IS preferred, and shall make an order accordingly.

**\[12A. Order or direction when to be made\].-** The Authority or the Court, as the case may be, after the case has been heard, shall make the order or direction either at once or, as soon thereafter as may be practicable, on some future day; and when the order or direction is to be made on some future day, it shall fix a date for the purpose of which due notice shall be given to the parties or their pleaders.\]

**13\. Inspection of documents.-**Any employed person, or any employer or his representative, or any person permitted under sub-section (2) of s.15 to apply for a direction, shall be entitled to inspect any application, memorandum of appeal, or any other document filed with the Authority or the Court, as the case may be, in a case to which he is a party and may obtain copies thereof on the payment of such fees as may be prescribed.

5.

Substituted for "record of direction in Form 'F' by the Payment of Wages (Procedure) (Amendment) Rules, 1959.

6.

Substituted for "a record of direction", _ibid._

7.

Subs. by ibid.

8.

Subs. for "direction", _ibid._

9.

Ins. by the Payment of Wages (Procedure) (Amendment) Rules, 1970.

**13\. Inspection of documents.-**Any employed person, or any employer or his representative, or any person permitted under sub-section (2) of s.15 to apply for a direction, shall be entitled to inspect any application, memorandum of appeal, or any other document filed with the Authority or .the Court, as the case may be, in a case to which he is a party and may obtain copies thereof on the payment of such fees as may be prescribed.

**FORM A**  
\[_See_ sub-section (2) of section 15 of the Payment of Wages Act\]

**FORM OF INDIVIDUAL APPLICATION**

In the Court of the Authority appointed under the Payment or Wages Act, 1936 (4 of 1936) for**.** area.  
**..**  
Application No**.** of 199  
Between A.B.C**.** Applicant  
(through **.** a legal practitioner/an official of**...** which is a registered Trade Union.)  
And X.Y.Zopposite party:  
The applicant states as follows:

 

 

in

 

factory

 

1.

A.B.C. is a person employed

\----

the

\----------

entitled

 

 

on

 

railway

 

 

 

industrial

 

establishment

 

 

 

 

 

 

 

 

and resides at **..**  
The address of the applicant for the service of all notices and processes is:  
2\. X. Y. Z., the opposite party, is the person responsible for the payment of his wages under section 3 of the Act, and his address .for the service of all notices and processes is:  
3\. (1) The applicant's wages have not been paid for the following wage-period(s) \[give (dates)\]  
Or A sum of Rs has been unlawfully deducted from his wages  
of amount for the wage-period(s) which ended on \[_give date(s)_\]  
(2) \[_Here give any further claim or explanation\]_.  
4\. The applicant estimates the value of the relief sought by him at the sum of Rs **...**  
5\. The applicant prays that a direction may be issued under sub-section (3) of section 15 for:  
(a) Payment of delayed wages as estimated or such greater or lesser amount as the Authority may find to be due.  
_Or_ Refund of the amount illegally deducted.  
(b) Compensation amounting to  
The applicant certifies that the statement of facts contained in this application is to the best of his knowledge and belief accurate.  
_Signature or thumb impression of the_  
_employed person, or legal practitioner or official_  
_of a registered trade union duly authorised._

**FORM B**

\[_See_ sub-section (2) of sections 15 and 16 of Payment of Wages Act\]

**FORM OF GROUP APPLICATION**

In the Court of the Authority appointed under the Payment of Wages Act, 1936 (4 of 1936) for**.** area  
Application **..**No of 199**.**  
_Between_ A. B. C. and (state the number)others,  
applicants;  
a legal practitioner

(through**.....** an official of  
**..** which is a registered trade union).  
_And_ X. Y. Z**.** opposite party.  
The applicants state as follows:

\[1. The applicants whose names and permanent addresses\] appear

<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse; border:medium none"><tbody><tr><td style="width:3.2in">&nbsp;</td>
<td style="width:31.5pt">in</td>
<td style="width:27.0pt">&nbsp;</td>
<td style="width:45.0pt">factory</td>
<td style="width:76.5pt">&nbsp;</td>
<td style="width:.45in">&nbsp;</td>
</tr><tr><td style="width:3.2in">in the attached schedule are persons employed</td>
<td style="width:31.5pt"><strong>------</strong></td>
<td style="width:27.0pt">the</td>
<td style="width:45.0pt"><strong>---------</strong></td>
<td style="width:76.5pt">&nbsp;</td>
<td style="width:.45in">&nbsp;</td>
</tr><tr><td style="width:3.2in">&nbsp;</td>
<td style="width:31.5pt">on</td>
<td style="width:27.0pt">&nbsp;</td>
<td style="width:45.0pt">railway</td>
<td style="width:76.5pt">&nbsp;</td>
<td style="width:.45in">&nbsp;</td>
</tr><tr><td style="width:3.2in">&nbsp;</td>
<td colspan="2" style="width:58.5pt">industrial</td>
<td colspan="2" style="width:121.5pt">establishment</td>
<td style="width:.45in">&nbsp;</td>
</tr></tbody></table>
 

in

 

factory

 

 

in the attached schedule are persons employed

**\------**

the

**\---------**

 

 

 

on

 

railway

 

 

 

industrial

establishment

 

entitled  
and resides at The address of the applicants for service of all notices and processes is:  
2\. X. Y. Z, the opposite party, is the person responsible for the payment of wages under section 3 of the Act, and his address for the service of all notices and processes is :  
3\. The applicants' wages have not been paid for the following wage-period(s):  
4.The applicants estimate the value of the relief sought by them at the sum of Rs  
5\. The applicants pray that a direction may be issued under sub-section (3) of section 15 for:  
(a)Payment of the applicants' delayed wages as estimated **..** or such greater or lesser amount as the Authority may find to be due.  
(b)Compensation amounting to  
The applicants certify that the statement of facts contained in this application is, to the best of their knowledge; and belief, accurate.  
_Signature or thumb impression of two of the_  
_applicants, or legal practitioner, or an official of_  
_a registered trade union duly authorised._

SCHEDULE

S.NO.  
1

Name of Applicant  
2

Permanent address  
3

 

 

 

 

 

 

10.

Subs. for the "The applicants whose names" by the Payment of Wages (Procedure) (Amendment) Rules, 1960.

11.

Subs. by the Payment of Wages (Procedure) (Amendment) Rules 1960.

**FORM C**  
\[_See_ sub-section (2) of sections 15 and 16 of the Payment of Wages Act\]  
**FORM OF APPLICATION BY AN INSPECTOR OR PERSON PERMITTED BY THE AUTHORITY OR**  
**AUTHORISED TO ACT**

In the Court of Authority appointed under the Payment of Wages Act, for **.......** area.  
Application No of 199  
_Between_  
A. B. C. \[(_designation_) an Inspector under the  
Payment of Wages Act\] or a person permitted by the authority/authorised to act under sub-section  
(2) of section 15\]**..** applicant.  
_And_  
X.Y.Z the opposite party.  
The applicant states as follows:  
1\. X. Y. Z., the opposite party is the person responsible under the Act for the payment of wages to the following \[persons whose names and permanent addresses are given below\]:  
(1)  
(2)  
(3)  
\*  
\*  
2\. His address for the service of all notices and processes is:  
3\. The wages of the said person(s) due in respect' of the following wage-period(s) have not been paid/have been subjected to the following illegal deductions:  
4\. The applicant estimates the value of the relief sought for the person(s) employed at the sum of Rs**.**  
5\. The applicant prays that a direction may be issued under sub-section (3) of section 15 for:  
(a)Payment of the delayed wages as estimated or such greater or lesser amount as the Authority may find to be due.

Or Refund of the amount illegally deducted.

(b)Compensation amounting to Rs**.**  
The applicant certifies that the statement of facts contained in this application is, to the best of his knowledge and belief, accurate.  
_signature_

**FORM D**  
**Certificate of Authorisation**

I/We employed person(s) hereby authorise a legal practitioner / an official of which is a registered trade union to act on my/ our behalf under section 15 and section 17 of the Payment of Wages Act, 1936 (4 of 1936), in respect of the claim against on account of the delay in payment illegal deductions from my / our wages for

Witnesses (1) Signatures(1)  
(2) (2)  
(3) (3)

12.

Subs. for "person(s)" by Payment of Wages (Procedure) (Amendment) Rules, 1960.

(4)  
\* \*  
\* \*  
I accept the authorisation.  
_Signature_  
_Legal practitioner/_  
_Official of a registered trade union_  
**FORM E**  
**NOTICE FOR THE DISPOSAL OF APPLICATION**  
To  
Whereas under the Payment of Wages Act, 1936 (4 of 1936) a claim against you has been presented to me in the application of which a copy is enclosed, you are hereby called upon to appear before me either in person or by any person duly instructed, and able to answer all material questions relating to the application, or who shall be accompanied by some person able to answer all material questions relating to the application, or who shall be accompanied by some person able to answer all such questions, on theday of**.** 199 at o'clock in the forenoon/ afternoon to answer the claim; and as the day fixed for your appearance is appointed for the final disposal of the application, you must be prepared to produce on that day all the witnesses upon whose evidence, and the documents upon which you intend to rely in support of your defence.  
Take notice that, in default of your appearance on the day beforementioned, the application will be heard and determined in your absence.  
Given under my hand and seal, this day of **..** 19  
Seal _Authority_

**FORM F**  
\[RECORD OF ORDER OR DIRECTION\]  
(1)Serial number  
(2)Date of the application **.**  
(3)Name or names, parentage, address or addresses of the applicant, or some, or all of the applicants belonging to the same unpaid group:  
(4)Name and address of the employer:

(5)Amount claimed:  
(a)as delayed wages: Rs  
(b)as deducted from wages: Rs  
(6)Plea of the employer and his examination (if any):

\[(7) Finding, and a brief statement of the reasons therefor\]:  
(8)Amounts awarded:  
(a)delayed wages Rs **.**  
(b)deducted wages  
(9)Compensation awarded  
(10)Penalty imposed  
 

13.

Subs. for "Record of Direction" by the Payment of Wages (Procedure) (Amendment) Rules,1959

14.

Subs. _ibid._

(11) Costs awarded to:  
(a)Court-fee charges **.**  
(b)Pleader's fee  
(c)Witnesses' expenses **..**  
\[(12) Date by which the amounts awarded sl1all be paid.\]  
Signed **..**  
Dated **...**  
**_Note_**_:_ In case where an appeal lies, attach on a separate sheet the substance of the evidence.

**FORM G**

**NOTICE TO RESPONDENT OF THE DAY FIXED FOR THE HEARING OF**  
**THE APPEAL UNDER SECTION 17 OF THE PAYMENT**  
**OF WAGES ACT, 1936**

Appeal from the decision of the Authority for the area dated the**..** day of**.** 199

To  
_Responder_  
Take notice that an appeal of which a copy is enclosed from the decision of the Authority for area has been presented by X.Y. Z. (and others), and registered in this Court, and that the**...** day of 199**..** , has been fixed by this court for the hearing of the appeal.

If no appearance is made on your behalf by yourself, or by some one by law authorised to act for you in this appeal, it will be heard and decided in your absence.

Given under my hand and the seal of the court, this **..** day of199

_Judge_  
Seal of the  
Court

15.

Ins. by Payment of Wages (Procedure) (Amendment) Rules, 1970.
