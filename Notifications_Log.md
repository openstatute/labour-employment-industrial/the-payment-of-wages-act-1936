# Notifications Log


## Act Ammendments

<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## Rule Ammendments
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## Orders
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## Circulars
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## Ordinance
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## Regulations
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

## 
<table>
  <tc>
    <th>S. No.</th>
    <th>Directory Located</th>
    <th>Ministry / Organization</th>
    <th>Department</th>
    <th>Office</th>
    <th>Subject</th>
    <th>Category</th>
    <th>Part & Section</th>
    <th>Issue Date</th>
    <th>Published in</th>
    <th>Published Date</th>
    <th>Issue No:: Page No</th>
    <th>Gazette ID</th>
  </tc>
  <tr>
    <td>00</td>
    <td>[Act](/main/Notifications_Log.md)</td>
    <td>Ministry of Railways</td>
    <td>Construction Department</td>
    <td>South Western Railway (Construction Department) Cantonment Bangalore</td>
    <td>Publication of Notification in the Extra Ordinary Gazette of India under Railway Act 1989.</td>
    <td>Extra Ordinary</td>
    <td>Part II-Section 3-Sub-Section(ii)</td>
    <td>00</td>
    <td>Extra Ordinary Gazette of India</td>
    <td></td>
    <td></td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
  <tr>
    <td>00</td>
    <td>[Notifications](/main/Notifications_Log.md)</td>
    <td>Ministry of xxx</td>
    <td>xxx Department</td>
    <td>Tamil Nadu State Election Commission, Chennai-600 106.</td>
    <td>[Elections - Ordinary Elections, 2019 – Tiruvarur District – Election of Ward No.11 of Tiruvarur District Panchayat – Death of a Candidate set up by Recognised Political Party – Adjourned – Cancellation Notification Issued.]</td>
    <td>Extra Ordinary</td>
    <td>Part VI—Section 2</td>
    <td>13th September 2021</td>
    <td>Extraordinary issues of the Tamil Nadu Government Gazette</td> <!-- Category -->
    <td></td>
    <td> 411:: 01 </td>
    <td>CG-KA-E-01102021-230111</td>
  </tr>
</table>

