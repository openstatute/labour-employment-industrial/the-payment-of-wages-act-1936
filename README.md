# The Payment of Wages Act 1936 

`HELP:
This is something to be warned about.
`  
The Payment of Wages Act, 1936 and its rules and ammendments

|  Act ID:   | Act Number:   | Enactment Date:  |Act Year:    | Ministry:  | Enforcement Date: |
|---|---|---|---|---|---|
|  193604  |  	04       | 1936-04-23   | 1936   |  Ministry of Labour and Employment | 28-03-1937 |

|  Long Title:   |
|---|
|  An Act to regulate the payment of wages of certain classes of employed persons.  |  



## About This Act

THE PAYMENT OF WAGES ACT, 1936 (ACT 4 OF 1936)

INTRODUCTION

With the growth of industries in India, problems relating to payment of wages to persons employed in industry took an ugly turn. The Industrial units were not making payment of wages to their workers at regular intervals and wages were not uniform. The industrial workers were forced to raise their heads against their exploitation.

The Payment of Wages Bill, 1935, based upon the same principle as the earlier Bill of 1933 but thoroughly revised was introduced in the Legislative Assembly on 15th February, 1935. The Bill was referred to the Select Committee. The Select Committee presented its report, with the amended Payment of Wages Bill, 1935 to the Legislative Assembly on 2nd September, 1935.

STATEMENT OF OBJECTS AND REASONS

In 1926 the Government of India addressed Local Governments with a view to ascertain the position with regard to the delays which occurred in the payment of wages to persons employed in industry, and the practice of imposing fines on them. The investigations revealed the existence of abuses in both directions and the material collected was placed before the Royal Commission on Labour which was appointed in 1929. The Commission collected further evidence on the subject and the results of their examination with their recommendations will be found on pages 216--221 and 236--241 of their Report. The Government of India re-examined the subject in the light of the Commission's Report and in February 1933 a Bill embodying the conclusions then reached was introduced and circulated for the purpose of eliciting opinion. A motion for the reference of the Bill to a Select Committee was tabled during the Delhi session of 1933 34, but was not reached, and the Bill lapsed, The present Bill is based upon the same principles as the original but has been revised throughout in the light of the criticisms received when the original Bill was circulated.

The Payment of Wages Bill, 1935 was passed by the Legislative Assembly and it received the assent on 23rd April, 1936. It came on the Statute Book as "THE PAYMENT OF WAGES ACT, 1936 (4 of 1936)".

## Central  
### Act  
<dl>  <!--Act Table -->
  <dt>[The Payment of Wages Act, 1936](#) [(Gazatte)](#)</dt>  
  <dd>The Payment of Wages (AMENDMENT) Act, 2005 [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 2017 [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1982 (38 of 1982)[(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1977 (19 of 1977) [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1976 (29 of 1976)[(Gazatte)](#)</dd>
  <dd>The Repealing and Amending Act, 1974 (56 of 1974) [(Gazatte)](#)</dd>
  <dd>The Central Labour Laws (Extension to Jammu and Kashmir) Act, 1970 (51 of 1970) [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1964 (53 of 1964) [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1957 (68 of 1957) [(Gazatte)](#)</dd>
  <dd>The Part B States (Laws) Act, 1951 (3 of 1951) [(Gazatte)](#)</dd>
  <dd>The Adaptation of Laws Order, 1950 [(Gazatte)](#)</dd>
  <dd>The Indian Independence (Adaptation of Central Acts and Ordinances) Order, 1948 [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Ordinance, 1940 (3 of 1940) [(Gazatte)](#)</dd>
  <dd>The Payment of Wages (Amendment) Act, 1937 (22 of 1937) [(Gazatte)](#)</dd>
  <dd>The Government of India (Adaptation of Indian Laws) Order, 1937[(Gazatte)](#)</dd>
  <dd>The Repealing and Amending Act, 1937 (20 of 1937)[(Gazatte)](#)</dd>
</dl>

### Rule  
<dl>  <!--Rule Table -->
  <dt>[The Payment of Wages (Procedure) Rules, 1937](#) [(Gazatte)](#)</dt>
    <!-- Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I,p.303. -->
  <dd>Payment of Wages (Procedure) (Amendment) Rules, 1951 [(Gazatte)](#)</dd>
  <dd>Payment of Wages (Procedure) (Amendment) Rules, 1959 [(Gazatte)](#)</dd>
  <dd>Payment of Wages (Procedure) (Amendment) Rules, 1970 [(Gazatte)](#)</dd>
  <dd>Payment of Wages (Procedure) (Amendment) Rules, 1960 [(Gazatte)](#)</dd>
  <dt>[Payment of Wages (Manner of Recovery of Excess Deductions) Rules, 1980](#) [(Gazatte)](#)</dt>
  <dt>[Payment Of Wages (Manner Of Recovery Of Excess Deductions) Rules, 1966](#) [(Gazatte)](#)</dt>
    <!-- S.O. 3090, dated 7.10.1966, published in the Gazette of India, Part II, Section 3(ii), dated 15.10.1966. -->
  <dt>[Payment of Wages (Procedure) Application to Scheduled Employment Rules, 1962](#) [(Gazatte)](#)</dt>
    <!--Notification G.S.R. 1569, dated 17.11.1962, published in the Gazette of India, Part 2, Section 3, dated 24.11.1962. -->
  <dt>[Payment of Wages (Mines) Rules, 1956](#) [(Gazatte)](#)</dt>
    <!-- Notification S.R.O. dated 30.11.1956, published in the Gazette of India, Part 2, Section 3, dated 8.12.1956 -->
  <dt>[Payment Of Undisbursed Wages (Mines) Rules, 1989](#) [(Gazatte)](#)</dt>
    <!-- G.S.R. 34(E), dated 18.1.1989, published in the Gazette of India, Extraordinary, Part II, Section 3(i), dated 18.1.1989. -->
  <dt>[Payment of Wages (Air Transport Services) Rules, 1968](#) [(Gazatte)](#)</dt>
    <!-- S.O. 3036, dated 5.8.1968, published in the Gazette of India, Part II, Section 3(ii), dated 7.9.1968. -->
  <dt>[Payment Of Undisbursed Wages (Air Transport Services) Rules, 1988](#) [(Gazatte)](#)</dt>
    <!-- G.S.R. 1208(E), dated 23.12.1988, published in the Gazette of India, Extraordinary, Part II, Section 3(i), dated 26.12.1988. -->  
  <dt>[Payment of Wages (Deductions for National Defence Fund and Defence Saving Scheme) Rules, 1980](#) [(Gazatte)](#)</dt>
  <dt>[Payment Of Wages (Deductions For National Defence Fund And Defence Savings Scheme) Rules, 1972](#) [(Gazatte)](#)</dt>
    <!-- G.S.R. 1346, published in the Gazette of India, Part II, Section 3(i), dated 21.11.1972 -->
  <dt>[Payment of Wages (Deductions for National Defence Fund and Defence Savings Schemes) Rules, 1963](#) [(Gazatte)](#)</dt>
    <!-- Noitfication S.O. 3376/PW A/ Section 7(2) (ii)/Rules, dated the 22nd November, 1963. -->
  <dt>[Payment of Wages (Nomination) Rules, 2009](#) [(Gazatte)](#)</dt>
    <!--Notification Gazette of India, Extra; Part 2, Section 3(i), dated 13th November, 2009.-->
  <dt>[Payment of Wages (Railways) Rules, 1938](#) [(Gazatte)](#)</dt>
    <!--Notification No.L. 3070(1), dated 5.5.1938, published in the Gazette of India, 1938, Part 1, page 943-->
</dl>  

## State and UT 

#### Andaman and Nicobar Islands(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Andhra Pradesh

<dl>
<dt>[Andhra Pradesh Payment of Wages Rules, 1937](#) [(Gazatte)](#)</dt>
</dl>

#### Arunachal Pradesh
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Assam
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Bihar

<dl>
<dt>[Bihar Payment of Wages Rules, 1937](#) [(Gazatte)](#)</dt>
</dl>

#### Chandigarh(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Chhattisgarh
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Dadra Nagar Haveli and Daman Diu(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Delhi(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Goa
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Gujarat

<dl>
<dt>[Gujarat Payment of Wages (Unclaimed Amounts) Rules, 1963](#) [(Gazatte)](#)</dt>
<dt>[Gujarat Payment of Wages Rules, 1963](#) [(Gazatte)](#)</dt>
</dl>

#### Haryana
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Himachal Pradesh
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Jammu and Kashmir(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Jharkhand
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Karnataka

<dl>
<dt>[Karnataka Payment of Wages Rules, 1963](#) [(Gazatte)](#)</dt>
</dl>

#### Kerala

<dl>
<dt>[Kerala Payment of Wages (Deductions for National Defence Fund & Defence Savings Schemes) Rules, 1964](#) [(Gazatte)](#)</dt>
<dt>[Kerala Payment of Wages (Manner of Recovery of Excess Deductions) Rules, 1968](#) [(Gazatte)](#)</dt>
<dt>[Kerala Payment of Wages (General) Rules, 1958](#) [(Gazatte)](#)</dt>
<dt>[Kerala Payment of Wages (Procedure) Rules, 1958](#) [(Gazatte)](#)</dt>
</dl>

#### Lakshadweep(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Ladakh(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Madhya Pradesh

<dl>
<dt>[Madhya Pradesh Payment of Wages Rules, 1962](#) [(Gazatte)](#)</dt>
</dl>

#### Maharashtra
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Manipur
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Meghalaya
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Mizoram
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Nagaland
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### National Capital Territory (Delhi)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Odisha

<dl>
<dt>[Odisha Payment of Wages Rules, 1936](#) [(Gazatte)](#)</dt>
</dl>

#### Puducherry(UT)
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Punjab

<dl>
<dt>[Punjab Payment of Wages Rules, 1937](#) [(Gazatte)](#)</dt>
</dl>

#### Rajasthan

<dl>
<dt>[Rajasthan Payment of Wages (Procedure) Rules, 1961](#) [(Gazatte)](#)</dt>
<dt>[Rajasthan Payment of Wages (Manner of Recovery of Excess Deductions) Rules, 1986](#) [(Gazatte)](#)</dt>
<dt>[Rajasthan Payment of Wages (Unclaimed Amounts) Rules, 1972](#) [(Gazatte)](#)</dt>
</dl>

#### Sikkim
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Tamil Nadu

<dl>
<dt>[Tamil Nadu Payment of Wages (Deductions for National Defence Fund and Defence Savings Scheme) Rules, 1973](#) [(Gazatte)](#)</dt>
<dt>[Tamil Nadu Payment of Wages (Unclaimed Amounts) Rules, 1949](#) [(Gazatte)](#)</dt>
</dl>

#### Telangana
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Tripura
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Uttar Pradesh
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#)</dt>
<dd>[](#)</dd>
</dl>
-->

#### Uttarakhand

<dl>
<dt>[Uttar Pradesh Payment of Wages (Procedure) Rules, 1958](#) [(Gazette)](#)</dt>
<dt>[Uttar Pradesh Payment of Wages (Procedure) Applicable to Scheduled Employment Rules, 1963](#) [(Gazatte)](#)</dt>
<dt>[Uttar Pradesh Payment of Wages Rules, 1936](#) [(Gazatte)](#)</dt>
</dl>

#### West Bengal
_Help us to find the rules_
<!-- 
<dl>
<dt>[](#) [(Gazatte)](#)</dt>
<dd>[](#) [(Gazatte)](#)</dd>
</dl>
-->

<!-- Model Table Structure-->
<!--
_Help us to find the state rules_
<dl>
<dt>[](#) [(Gazatte)](#)</dt>
<dd>[](#) [(Gazatte)](#)</dd>
</dl>
-->



---


## State Notifications 









**Notification Date:  26-06-1996**

**Notification Title:  Tamilnadu-Extension of provisions of Payment of Wages Act to shops and commercial establishments employing 20 or more persons. 26th June, 1996**

**Notification Description:  Government of  Tamil NaduNo. II(2)JLE/1 624/96. In exercise of the powers conferred by sub- clause (h) of clause (ii) of section 2 of the Payment of Wages Act, 1936 (Central Act IV of 1936), the Governor of Tamil Nadu having regard to ...**

**Notification Issues By: State Government**

[Read More](actreadmore?secid=rRUroo3CwKkwBsA/8140hndy9fUIJVOJev%2B0hgNC/bc%3Dactid=cOwuFhbXPAcknoMPWwSkgTxARvVqCnsty2j6qQ7EaYs%3D)

05-08-1981

Declaration of newspaper establishments as industrial establishments for purpose of Payment of Wages Act. 5th August 1981

Government of  Tamil Nadu(No. II(2)/LE/3612/81) In exercise of the powers conferred by item (h) of clause (ii) of section 2 of the Payment of Wages Act, 1936 (Central Act IV of 1936), the Governor of Tamil Nadu hereby declares the Newspaper E ...

State Government

[Read More](actreadmore?secid=5LAzOGzZyhHWxQN51/nezS7UoMG6LeGy3a%2BuKuRVFwA%3D&actid=HMfSJ3xjPvtQpHB9fhiwUBTlxE5fMQIRbneaDALTySU%3D)

24-01-1986

Tamilnadu -G.O. Ms. No. 128, Labour, 24th January, 1986, Thai 11, Kurothana, Thiruvalluvar Aandu - 2017.

  Government of  Tamil Nadu      No. II (2)/LAB/1135/86 In exercise of the powers conferred by sub-clause (h) of clause (ii) of section 2 of the Payment of Wages Act, 1936 (Central Act IV of 1936), the Governor o ...

State Government

[Read More](actreadmore?secid=/vv7PfpEIeTKeIzWSbzXnmaaxrehb3pHLR9QXwVjsyY%3D&actid=eD8jXaOUlxYPekg5U8mjw8OpmcM01KtPEUqiHdkupEw%3D)

07-07-2013

kerala -Industrial Employment Standing Orders Act made applicable to all ^commercial establishments^ situated in Kerala

GOVERNMENT OF KERALA  Notification No:- G. 0. (P) No. 74/2013/11W.Date:-07.06.2013S. R. 0. No. 485/2013—in exercise of the powers conferred by sub-clause (h) of clause (ii) of section 2 of the Payment of wages Act, 1936 (Central Act 4 o ...

State Government

[Read More](actreadmore?secid=V57B0fa0cofuW8Pn7JjxrPoWdqgbPkhVnMC5b7BPxA0%3D&actid=YHRPmx4vUpepNgDVpWBPidzUyYBzypMeAAQr3lPPt6c%3D)

07-06-2013

Kerala Payment of wages Act made applicable to all ^commercial establishments^ {U. Sec.2 (h) of PW Act}

GOVERNMENT OF KERALA  Notification No:- G. 0. (P) No. 74/2013/11W.Date:-07.06.2013S. R. 0. No. 485/2013—in exercise of the powers conferred by sub-clause (h) of clause (ii) of section 2 of the Payment of wages Act, 1936 (Central Act 4 o ...

State Government

[Read More](actreadmore?secid=48Tc7fDj%2BfoPITT2gsqy7mQJzDRJ83jlT96bHEp075k%3D&actid=jVotgIF1SOfz5hLFeZsiu7zUcoNmUuAxmugJQhLKH0c%3D)

05-08-2014

Assam - Notification regarding 24 Establishments are declared as Industrial or other Establishment under Payment of Wages Act,1936

GOVERNMENT OF ASSAMLABOUR AND EMPLOYMENT DEPARTMENT  Notification No: - GLR (RC) 4/2014/21 Date: - 5/08/2014      In exercise of the powers conferred by section 1(5) of the payment of Wages Act, 1936 read wi ...

Central Government

[Read More](actreadmore?secid=Qf0iIUR9/Jq8RR2bWUXg4aqLru9YC5YwjgILakIHpNk%3D&actid=nLN5fM9kVPM21dUgVJCsfxsSUr97WCtxKoRtEf4k6Vg%3D)


## Maintaing Change Log

Change log is a file capturing macanism of all the changes made in this repository. 

1. While uploading a new file
2. while amending the exisiting files

### Procedure to maintain Change Log File


## Maintaing Notifications Log

Notifications log is a capturing macanism of all the notifications received from the government. All type of notifications pertinent to this subject matter will be uploaded and captured in this Log file. parallaly transfering the information into exisiting files. 

Always the notification shall be a Gazattee file. very rare circumstances only other than Gazattee file will be acceepted. to maintain the authenticity. 

1. While uploading a new notifications file
2. while amending the exisiting files with clear copy.

### Procedure to maintain Notifications Log File

